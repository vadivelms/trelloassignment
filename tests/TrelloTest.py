import time
import unittest

from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

from pageObjects.BoardPage import BoardPage
from pageObjects.CardPage import CardPage
from pageObjects.HomePage import HomePage
from pageObjects.LoginPage import LoginPage


class TrelloTests(unittest.TestCase):
    email = "vadivelmso@gmail.com"
    pwd = "testpass"
    boardName = "newboard1"

    @classmethod
    def setUpClass(self):
        print("setup")
        driver = webdriver.Chrome(ChromeDriverManager().install())
        driver.get("https://trello.com/login")
        driver.maximize_window()
        self.driver = driver

    # def tearDown(self):
    #     self.driver.close()

    def test_1_login(self):
        print("test_1_login")
        login_page = LoginPage(self.driver)
        login_page.enterUsername(TrelloTests.email)
        time.sleep(5)
        login_page.clickLogin()
        login_page.enterPassword(TrelloTests.pwd)
        login_page.clickSubmitLogin()

    def test_2_create_new_board(self):
        print("test_2_create_new_board")
        home_page = HomePage(self.driver)
        home_page.clickCreateBtn()
        home_page.clickCreateBoardBtn()
        home_page.enterBoardTitle(TrelloTests.boardName)
        time.sleep(5)
        home_page.clickcreatBoardSbmtBtn()
        time.sleep(5)

    def test_3_create_new_lists(self):
        print("test_3_create_new_lists")
        home_page = HomePage(self.driver)
        board_page = BoardPage(self.driver)
        try:
            home_page.clickAddListBtn()
        except:
            print("Add List Btn Not Found")
        board_page.enterListName("Not Started")
        board_page.clickAddListBtn()
        board_page.enterListName("In Progress")
        board_page.clickAddListBtn()
        board_page.enterListName("QA")
        board_page.clickAddListBtn()
        board_page.enterListName("Done ")
        board_page.clickAddListBtn()

    def test_4_create_new_cards(self):
        print("test_4_create_new_cards")
        card_page = CardPage(self.driver)
        card_page.clickAddCrdBtn()
        card_page.enterCardName("Card 1")
        card_page.clickAddCrdCnfmBtn()
        card_page.enterCardName("Card 2")
        card_page.clickAddCrdCnfmBtn()
        card_page.enterCardName("Card 3")
        card_page.clickAddCrdCnfmBtn()
        card_page.enterCardName("Card 4")
        card_page.clickAddCrdCnfmBtn()

    def test_5_move_card_two_to_in_progress(self):
        print("test_5_move_card_two_to_in_progress")
        card_page = CardPage(self.driver)
        card_page.clickCrd2lnk()
        card_page.clickMoveLnk()
        card_page.selectList("In Progress")
        card_page.clickMoveBtn()
        card_page.clickCloseBtn()

    def test_6_move_card_three_to_QA(self):
        print("test_6_move_card_three_to_QA")
        card_page = CardPage(self.driver)
        card_page.clickCrd3lnk()
        card_page.clickMoveLnk()
        card_page.selectList("QA")
        card_page.clickMoveBtn()
        card_page.clickCloseBtn()

    def test_7_move_card_two_to_QA(self):
        print("test_7_move_card_two_to_QA")
        card_page = CardPage(self.driver)
        card_page.clickCrd2lnk()
        card_page.clickMoveLnk()
        card_page.selectList("QA")
        card_page.clickMoveBtn()
        card_page.clickCloseBtn()

    def test_8_assign_card_one_to_current_user(self):
        print("test_8_assign_card_one_to_current_user")
        card_page = CardPage(self.driver)
        card_page.clickCrd2lnk()
        card_page.clickMembersLnk()
        card_page.enterComments("I am done")
        card_page.clickSaveBtn()
        card_page.clickCloseBtn()


if __name__ == '__main__':
    unittest.main()
