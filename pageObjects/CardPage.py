from selenium.webdriver.common.by import By
from utils.BasePage import BasePage
from selenium.webdriver.support.select import Select


class CardPage(BasePage):
    addCrdBtn = (By.XPATH, "(//span[text()='Add a card'])[1]")
    addCrdTitletxtAra = (By.CSS_SELECTOR, "textarea[placeholder='Enter a title for this card…']")
    addCrdCnfmBtn = (By.CSS_SELECTOR, "input[value='Add card']")
    moveBtn = (By.CSS_SELECTOR, "input[value='Move']")
    saveBtn = (By.CSS_SELECTOR, "input[value='Save']")
    moveLnk = (By.CSS_SELECTOR, "a[title='Move']")
    closeBtn = (By.XPATH, "//a[@class='icon-md icon-close dialog-close-button js-close-window']")
    membersLnk = (By.CSS_SELECTOR, "a[title='Members']")
    crd1lnk = (By.XPATH, "//span[contains(text(),'Card 1')]")
    crd2lnk = (By.XPATH, "//span[contains(text(),'Card 2')]")
    crd3lnk = (By.XPATH, "//span[contains(text(),'Card 3')]")
    listDrpDwn = (By.XPATH, "//label[text()='List']/following-sibling::select")
    currentUser = (By.XPATH, "//span[@class='full-name']")
    commentsTxtAra = (By.XPATH, "//textarea[@placeholder='Write a comment…']")

    def clickAddCrdBtn(self):
        element = self.driver.find_element(*CardPage.addCrdBtn)
        element.click()

    def enterCardName(self, name):
        element = self.driver.find_element(*CardPage.addCrdTitletxtAra)
        element.send_keys(name)

    def clickAddCrdCnfmBtn(self):
        element = self.driver.find_element(*CardPage.addCrdCnfmBtn)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        element.click()

    def clickMoveLnk(self):
        element = self.driver.find_element(*CardPage.moveLnk)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        element.click()

    def clickCrd2lnk(self):
        element = self.driver.find_element(*CardPage.crd2lnk)
        element.click()

    def selectList(self,listName):
        sel = Select(self.driver.find_element(*CardPage.listDrpDwn))
        sel.select_by_visible_text(listName)

    def clickMoveBtn(self):
        element = self.driver.find_element(*CardPage.moveBtn)
        element.click()
    def clickCloseBtn(self):
        element = self.driver.find_element(*CardPage.closeBtn)
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        element.click()


    def clickCrd3lnk(self):
        element = self.driver.find_element(*CardPage.crd3lnk)
        element.click()

    def clickCrd1lnk(self):
        element = self.driver.find_element(*CardPage.crd1lnk)
        element.click()

    def clickMembersLnk(self):
        element = self.driver.find_element(*CardPage.membersLnk)
        element.click()
    def clickCurrentUser(self):
        element = self.driver.find_element(*CardPage.currentUser)
        element.click()
    def clickSaveBtn(self):
        element = self.driver.find_element(*CardPage.saveBtn)
        element.click()

    def enterComments(self, comments):
        element = self.driver.find_element(*CardPage.commentsTxtAra)
        element.send_keys(comments)
