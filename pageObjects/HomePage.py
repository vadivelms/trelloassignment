from selenium.webdriver.common.by import By

from utils.BasePage import BasePage


class HomePage(BasePage):
    creatBtn = (By.CSS_SELECTOR, "button[data-test-id='header-create-menu-button']")
    creatBoardBtn = (By.CSS_SELECTOR, "button[data-test-id='header-create-board-button']")
    creatBoardSbmtBtn = (By.CSS_SELECTOR, "button[data-test-id='create-board-submit-button']")
    creatBoardTxtBox = (By.CSS_SELECTOR, "input[data-test-id='create-board-title-input']")
    addListBtn = (By.XPATH, "//span[contains(text(),'Add a list')]")

    def clickCreateBtn(self):
        element = self.driver.find_element(*HomePage.creatBtn)
        element.click()

    def clickAddListBtn(self):
        element = self.driver.find_element(*HomePage.addListBtn)
        element.click()

    def clickCreateBoardBtn(self):
        element = self.driver.find_element(*HomePage.creatBoardBtn)
        element.click()

    def clickcreatBoardSbmtBtn(self):
        element = self.driver.find_element(*HomePage.creatBoardSbmtBtn)
        element.click()

    def enterBoardTitle(self, title):
        element = self.driver.find_element(*HomePage.creatBoardTxtBox)
        element.send_keys(title)
