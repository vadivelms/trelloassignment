from selenium.webdriver.common.by import By
from utils.BasePage import BasePage


class BoardPage(BasePage):
    listNameTxtBox = (By.CSS_SELECTOR, "input[name='name']")
    addListBtn = (By.CSS_SELECTOR, "input[value='Add list']")

    def enterListName(self, name):
        element = self.driver.find_element(*BoardPage.listNameTxtBox)
        element.send_keys(name)

    def clickAddListBtn(self):
        element = self.driver.find_element(*BoardPage.addListBtn)
        element.click()