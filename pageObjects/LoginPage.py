from selenium.webdriver.common.by import By
from utils.BasePage import BasePage


class LoginPage(BasePage):
    user = (By.ID, 'user')
    pwd = (By.ID, 'password')
    login = (By.ID, 'login')
    submitLogin = (By.ID, 'login-submit')

    def enterUsername(self, username):
        userElement = self.driver.find_element(*LoginPage.user)
        userElement.send_keys(username)

    def enterPassword(self, password):
        userElement = self.driver.find_element(*LoginPage.pwd)
        userElement.send_keys(password)

    def clickLogin(self):
        clickLoginElement = self.driver.find_element(*LoginPage.login)
        clickLoginElement.click()

    def clickSubmitLogin(self):
        clickLoginElement = self.driver.find_element(*LoginPage.submitLogin)
        clickLoginElement.click()
